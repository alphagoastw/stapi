
var bcrypt = require("bcrypt-nodejs");
var createSendToken = require("./createSendToken.js");
var sessionTokenHelper = require("../services/sessionTokenHelper.js");
var emailTokenManager = require("../services/emailTokenManager.js");
var secret = "this is my secret";
var eamilSender = require("../services/emailSender/emailService.js");
var moment = require("moment");
var ObjectID = require('mongodb').ObjectID;

module.exports = {
  register: register,
  registerConfirm:registerConfirm,
  checkUsername: checkUsername,
  forgetPassword: forgetPassword,
  resetPassword: resetPassword,
  changePassword: changePassword,
  getTokenData: getTokenData,

  checkEmail: checkEmail,
  updateUserEmail:updateUserEmail,
  updateUserAccountUserNameAndPassword: updateUserAccountUserNameAndPassword
}

// this will send email
function forgetPassword(req, res){
   var email = req.param("email"); 
   var username = req.param("username"); 

   if(!email && !username) {
      return res.status(400).send("Bad data");
   }

   if(email && email.indexOf("@") < 0){
      return res.status(400).send("Bad email");
   }

   var condition = {
     
   }
   
   if(email) {
    condition.email = email;
   } 

   if(username) {
    condition.userName = username;
   }

   User.findOne(condition, function(err, userFound){
      if(err){
        sails.log.error("Error in forgetPassword, ", err); 
      }

      if(!userFound){
        return res.status(404).send("No such user");
      }

      sails.log.info("forgetPassword: sending forget password email:"); 

     // Send welcome email
     eamilSender.sendForgetPasswordEmail(userFound)
       .then(function (sendInfo) {
         sails.log.info("ForgetPassword:Sending forget password email successfully finished. Information:", sendInfo);
         return res.status(200).send();
       })
       .catch(function (err) {
         sails.log.info("ForgetPassword:Sending forget password  email error. Information:", err);
         return res.status(500).send(err);
       }); 
   }) 

}

function resetPassword(req, res){ 
  var token = req.body.token;
  var email = req.body.email; 
  var password = req.body.password;
  var passwordConfirm = req.body.passwordConfirm;



  sails.log.info("Try to find the token", req.body ); 

  if(!token || !email || !password || password != passwordConfirm){
    return res.status(400).send("Bad data");
  }

  sails.log.info("Try to find the token"); 

  Token.findOne({tokenGuid:token, email:email})
  .then(function(tokenData){
     if(!tokenData){
       return res.status(404).send("Not found");
     }


     bcrypt.genSalt(10, function (err, salt) {
          if (err){
              sails.log.error("reset password woring(1), error:", err);
                return res.status(500).send({
               message: "reset password wrong"
              });
           }

           bcrypt.hash(password, salt, null, function (err, hash) {
            if (err){
               sails.log.error("reset password woring(2), error:", err);
               return res.status(500).send({
                  message: "reset password wrong"
                });
              }


            //Now change 
             User.update({email:email}, {password:hash})
             .then(function(updated){
                 if(!updated){
                    return res.status(404).send("User not found");
                 }

                 // change succeed;  then delete the token from token table  
                tokenData.destroy();
                return res.status(200).send("User password changed"); 
         })   
      })  
    })  
  })  
  .catch(function(err){
    sails.log.error("Error happens, err:", data, ' data:', req.body);
    return res.status(404).send("Not found");
  }) 
}

function getTokenData(req, res) { 
  
  var token = req.param("token"); 
  if(!token) {
    return res.status(404).send("Not found");
  } 

  Token.findOne({tokenGuid:token})
  .then(function(tokenData){  
     if(!tokenData){
      return res.status(400).send("Not found");
     }

     delete tokenData.id; 
     return res.status(200).send(tokenData);
  })
  .catch(function(err){
    sails.log.error(err);
    res.status(404).send("Not found");
  })
}

function changePassword(req, res) {

  sails.log.info('inside change password, manuAuth_register.js');

  var userid = req.param('uid');
  var currentPassword = req.body.currentPassword;
  var newPassword = req.body.newPassword;
  var newPasswordConfirm = req.body.newPasswordConfirm;

  if (currentPassword === newPassword || newPassword !== newPasswordConfirm) {
    return res.status(400).send('Bad data');
  }

  User.findOne(userid, function (err, userFound) {
    if (!userFound) {
      sails.log.info("user with id=" + userid + " cannot be found");
      return res.status(200).send('false');
    }
    else {
      // check currentPassword
      bcrypt.compare(currentPassword, userFound.password, function (err, valid) {

        if (err || !valid) {
          return res.status(401).send({
            message: "Wrong current password"
          });
        }

        // set new password
        bcrypt.genSalt(10, function (err, salt) {
          if (err){
              sails.log.error("reset password woring(1), error:", err);
             return res.status(500).send({
               message: "reset password wrong"
              });
           }

           bcrypt.hash(newPassword, salt, null, function (err, hash) {
            if (err){
               sails.log.error("reset password woring(2), error:", err);
               return res.status(500).send({
                  message: "reset password wrong"
                });
              }

            userFound.password = hash;
            userFound.save(userFound, function (err, userUpdated) {

              if (err) {
                 sails.log.error(err, userUpdated);
                return res.status(500).send(err);
              } else { 
                return res.status(200).send(userUpdated);
              }

            })
         })
       })



      })
    }
  })
}

function registerConfirm(req,res){
   var token = req.param("token");
   var payload = emailTokenManager.getPayloadFromRegistrationWelcomeToken(token);

   if(payload && payload.exp <= moment().unix()){
     return res.status(400).send("token expired");
   } else {
   // update user emailConfirmed

   User.findOne({email:payload.email})
   .then(function(userFound){
         var signInTimes = userFound.signInTimes ?  userFound.signInTimes + 1: 1;

         User.update({email:payload.email}, {emailConfirmed:true, signInTimes:signInTimes})
           .exec(function(err, userUpdated){
              if(err){
                    res.status(500).send(err);
              } else {
              // Generate token and return
               userUpdated = [].concat(userUpdated);
               userUpdated = userUpdated[0];
               var token = sessionTokenHelper.createSessionToken(userUpdated, res);
               res.status(200).send({
               user: userUpdated,
               token: token
               });
            };
          })
      })
   }
}

function updateUserEmail(req,res) {
  var userid = req.param('uid');
  var newEmail = req.body.newEmail;
  var oldEmail = req.body.oldEmail;

  if (!username) {
    return res.status(200).send('false');
  }
  var userToUpdate = {
    email: email,
    oldEmail: oldEmail,
    newEmail: newEmail
  };

  User.findOne({email: oldEmail}, function (err, userFound) {
    if (!userFound) {
      return res.status(200).send('false');
    }
    else {
      if (req.session.userid !== userid) {
        return res.status(301).send("You cannot change other person's account");
      }

      User.findOne({email: oldEmail}, function (err, userByUserName) {
        if (!user || user.id == userid) {
          // The user name is not used, or the same person
          userFound.username = username;
          User.findOne({email: newEmail}, function (err, userByEmail) {
            if (!userByEmail || user.id == userid) {
              // The email address is not used, or the same email address
              userToUpdate.email = newEmail;

              userFound.save(userFound, function (err, userFound) {
                if (err) {
                  return defer.reject(err);
                }
                else{
                  defer.resolve(userFound)
                }
              })

            } else {
              return res.status(400).send('Email has been used');
            }
          })
        } else {
          return res.status(400).send('Username has been used');
        }
      })
    }
  })
}

function updateUserAccountUserNameAndPassword(req, res) {
  var userid = req.param('uid');
  var username = req.body.userName;
  var password = req.body.password;

  if (req.session.userid !== userid) {
    return res.status(301).send("You cannot change other person's account");
  }

  if (!username || !password) {
    return res.status(200).send('false');
  };

  User.findOneById(userid, function (err, userFound) {
    if (!userFound) { 
      return res.status(200).send('false');
    }
    else {

      User.findOne({userName: username}, function (err, userByName) {
        if(err){
          return res.status(500).send(err);
        }


        if (!userByName || userByName.id == userid) {
          // The user name is not used, or the same person

          userFound.userName = username;

          bcrypt.genSalt(10, function (err, salt) {
          if (err){
              sails.log.error("reset password woring(1), error:", err);
             return res.status(500).send({
               message: "reset password wrong"
              });
           }

           bcrypt.hash(newPassword, salt, null, function (err, hash) {
            if (err){
               sails.log.error("reset password woring(2), error:", err);
               return res.status(500).send({
                  message: "reset password wrong"
                });
              }

            userFound.password = hash;

            userFound.save(userFound, function (err, userUpdated) {
            if (err) {
              sails.log.error(err);
              return res.status(500).send(err);
            } else{ 
              return res.status(200).send(userUpdated);
            }})
          }) // end of bcrypt.hash
         })   // end of bcrypt.genSault
        }
        else {
          sails.log.error(userByName);
          return res.status(400).send('Username has been used');
        }
      })
    }
  });
}

function checkUsername(req,res){
  var username = req.params.username;
  if(!username){
    return res.status(200).send('false');
  };

  User.findOne({userName:username}, function (err, user) {
    if (!user) {
      return res.status(200).send('false');
    }
    else {
      return res.status(200).send('true');
    }
    ;
  })
}

function  checkEmail(req,res) {
  var email = req.params.email;
  if(!email){
    return res.status(200).send('false');
  };

  User.findOneByEmail(email, function (err, user) {
    if (!user) {
      return res.status(200).send('false');
    }
    else {
      return res.status(200).send('true');
    }
    ;
  })
};

function  register(req, res) {

  var email = req.body.email;
  var userName = req.body.userName;
  var password = req.body.password;
  var confirmPassword = req.body.confirmPassword;
  if (!email || !userName || !password || !confirmPassword || password != confirmPassword) {
    return res.status(400).send({
      message: "invalid data"
    });
  }

  var condition = {
    or:[
      {email:email},
      {userName:userName}
    ]
  };

 User.findOne(condition).exec(function(err, user) {

   if (err) {
     sails.log.error("Trying to find user by condition failed. Condition:", condition, " ,Error:", err);
     return res.status(400).send(err);
   }

   if (user) {
     sails.log.info(user);
     return res.status(400).send("User with the email or userName exits.");
   }

   // No user found
   sails.log.info("Can not find the user, create a new one");
   User.create({
     email: email,
     userName: userName,
     displayName: userName,
     password: password
   }).exec(function (err, user) {
     if (err) {
       sails.log.error("Create user failed,", user);
       sails.log.error("Create user failed,", err);
       return res.status(400).send(err);
     }

     sails.log.info("Registration: Create new user successfully finished. User:", user);

     // Send welcome email
     eamilSender.sendRegisterWelcomeEmail(user)
       .then(function (sendInfo) {
         sails.log.info("Registration:Sending invite email successfully finished. Information:", sendInfo);
       })
       .catch(function (err) {
         sails.log.info("Registration:Sending invite email error. Information:", err);
       });

     return res.status(200).send(user);
   });

 })
}
