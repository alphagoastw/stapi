var mailer = require('sails-service-mailer');
var mailerConfig = sails.config.mailer;
var config = require('../config.js');
var emailTokenManager = require("../../services/emailTokenManager.js");

var Q = require('q');
var _ = require('lodash');
var moment = require('moment');
var fs = require('fs');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');

var EmailService = function() {};

module.exports = new EmailService;

///------------------- implementation -----------------------------------------

EmailService.prototype.sendRegisterWelcomeEmail = sendRegisterWelcomeEmail;
EmailService.prototype.sendForgetPasswordEmail = sendForgetPasswordEmail;


_.templateSettings.variable = "rc";

function sendRegisterWelcomeEmail(user) {
    var emailInfo = getWelComeEmailData(user);
    // send email
    return sendEmail(emailInfo);
}

function sendEmail(emailInfo) {
    var emailType = emailInfo.emailType;
    var toEmail = emailInfo.recipients.join(',');
    var priorityLevel = emailInfo.priority || 'normal';

    var subject = emailInfo.subject;
    var transporter = nodemailer.createTransport(smtpTransport(mailerConfig));

    var emailTemplateModel = emailInfo.data;
    var templatePath = getEmailTemplate(emailType);
    var emailContent = getHtml(emailTemplateModel, templatePath);

    var mailOptions = {
        from: "故事大王 <wshuhao@gmail.com>",
        to: toEmail,
        subject: subject,
        html: emailContent,
        priority: priorityLevel
    };

    var deferred = Q.defer();

    sails.log('In MailerService.sendEmail.  Starting sending email....');

    transporter.sendMail(mailOptions, function(err, info) {
        if (err) {
            sails.log.error('Sending email failed. Error message:', err, ',email configuration:', emailConfig);
            return deferred.reject(err);
        }
        sails.log.info('Email sent successfully.', info);
        return deferred.resolve(info);
    });

    return deferred.promise;
}

function getWelComeEmailData(user) {
    var token = emailTokenManager.createRegistrationWelcomeToken(user);
    //TODO: remove the hard code email  
    user.email = "wshuhao@gmail.com";

    var recipient = [];
    recipient.push(user.email);

    var storyToasterUrl = sails.config.storyToaster.host;
    sails.log.info(storyToasterUrl);
    var link = storyToasterUrl + "/register/confirm?token=" + token;
    return {
        emailType: 'registerWelcome',
        subject: "故事大王注册确认邮件",
        recipients: recipient,
        data: {
            userName: user.userName,
            link: link
        }
    }
}

function getForgetPasswordEmailData(user) {

    //TODO 
    user.email = "wshuhao@gmail.com";
    var recipient = [];
    recipient.push(user.email);

    var storyToasterUrl = sails.config.storyToaster.host;
    sails.log.info(storyToasterUrl);
    var link = storyToasterUrl + "/account/resetPassword?token=" + user.token;

    return {
        emailType: 'forgetPassword',
        subject: "故事大王账户密码重置邮件",
        recipients: recipient,
        data: {
            userFullName: user.displayName,
            userName: user.userName,
            email: user.email,
            link: link
        }
    }
}

function sendForgetPasswordEmail(user) {

    var guidtoken = emailTokenManager.getGuidToken(); 

    return Token.create({
        tokenGuid: guidtoken,
        email: user.email
    })
    .then(function(token){ 
       sails.log.info("Forget password: Create token successfully finished. User:", user);  
       user.token = guidtoken;
       var data = getForgetPasswordEmailData(user);
       return sendEmail(data); 
    })
}

function getEmailTemplate(emailType) {
    if (emailType == 'registerWelcome') {
        return "./api/template/email/register-welcome.html";
    } else if (emailType == 'forgetPassword') {
        return "./api/template/email/forget-password.html";
    } else if (emailType == 'buyBookOrder') {
        return "./api/template/email/buy-book-order.html";
    }
}

function getHtml(model, templatePath, encoding) {
    var html = fs.readFileSync(templatePath, encoding = "utf8");
    var template = _.template(html);
    return template(model);
}