var mediaTokenHelper = require("../tokenHelper.js");

var fs = require('fs'),
    util = require('util');

var jimp = require('jimp');
var path = require("path");
var root = require('app-root-path') + "";

//var Canvas = require('canvas');

var cache = Object(); // {}

module.exports = {
    getPersonalImages: getPersonalImages,
    getImages: getImages,
    serveImage: serveImage,
    serveBookPageImage:serveBookPageImage,
    playVideo: playVideo
};

function serveBookPageImage(req, res) {
  //var pageId = req.param('pageId');
  //var width = req.param('width');
  //var height = req.param('height');
  //var imageSize = req.param('size');
  //
  //BookPage.find({id: pageId}).populate("book")
  //  .then(function (page) {
  //    if (!page) {
  //      return res.notFound()
  //    }
  //
  //    var pageWidth = page.book.frontCover.width;
  //    var pageHeight = page.book.frontCover.height;
  //
  //    var canvas = new Canvas(width, height);
  //    canvas.loadJSON(page.imageData, function () {
  //
  //      var base64Image = canvas.toDataURL();
  //      var originFile = file.mediaFile;
  //
  //      if (imageSize === 'origin') {
  //        if (originFile)
  //          base64Image = originFile.data;
  //      } else {
  //        width = parseInt(width);
  //        height = parseInt(height);
  //        //-------------------------------------
  //        sails.log.info("resize image");
  //        var img = new Buffer(originFile.data);
  //        var imgType = "PNG";
  //
  //        jimp.read(img, imgType)
  //          .then(function (image) {
  //            var originSizeWidth = image.bitmap.width;
  //            var originSizeHeight = image.bitmap.height;
  //            var targetWidth = width;
  //            var targetHeight = ( width / originSizeWidth) * originSizeHeight;
  //            base64Image = image.resize(targetWidth, targetHeight);
  //          })
  //      }
  //
  //      var img = new Buffer(base64Image, 'base64');
  //
  //      res.writeHead(200, {
  //        "Content-Type": imgType,
  //        "Content-Length": img.length
  //      });
  //
  //      res.end(img);
  //    })
  //  },
  //  function (err) {
  //    res.serverError(err);
  //  })
  //  .catch(function (err) {
  //    res.serverError(err);
  //  })
}

function getPersonalImages(req, res) {
    if (req.session.userid == null && category === 'photo') {
        return res.ok(null);
    } else {
        return getImages(req, res);
    }
};

function getImages(req, res) {
    var details = req.param('details');
    var category = req.param('cat');
    var pageNumber = req.param("pageNumber");
    var pageSize = req.param("pageSize");
    var orderBy = req.param("orderBy");
    var random = req.param("random");
    var theme = req.param("theme");
    var tag = req.param("tag");

    if (!pageSize || pageSize > 100 && req.session.role != 'admin') {
        pageSize = 100;
    }

    if (!pageNumber) {
        pageNumber = 0;
    }

    if (!orderBy) {
        orderBy = 'thirdPartyId, createdAt DESC, theme'
    }

    var condition = {};
    if (category === 'background' || category === 'props' || category === 'bigText' ||
        category === 'text' || category === 'personal' || category === 'page' || category === 'profileIcon') {
        condition = {
            category: category,
            status: "enabled"
        }
    }

    if (category === 'personal') {
        condition.owner = req.session.userid;
    }

    if (theme && theme != 'undefined') {
        condition.theme = theme;
    }

    if (tag && tag != 'undefined') {
        condition.tag = { contains: tag }
    }

    var select = {
        select: ['id', 'tag', 'category', 'width', 'height',
            'contentType', 'fileSize'
        ]
    };

    if (details) {
        var ext = ['status', 'source', 'thirdPartyId', 'theme', 'createdAt'];
        select.select = select.select.concat(ext);
    }

    if (category == 'text' || category == 'bigText' || category == 'profileIcon') {
        random = false;
    }

    var skip = pageNumber * pageSize;

    Media.find({ category: category, status: "enabled" }, { select: ['id'] })
        .then(function(ids) {
            return ids;
        })
        .then(function(ids) {
            var count = ids.length;

            var promise;
            if (random !== 'true') {
                sails.log.info('not in random mode..., skip:', skip, " limit:", pageSize);
                promise = Media.find(condition, select, orderBy)
                    .skip(skip).limit(pageSize);

            } else {

                sails.log.info('in random mode...');

                var ids = _.shuffle(ids).splice(0, pageSize).map(function(id) {
                    return id.id;
                });

                condition = {
                    id: ids
                };

                promise = Media.find(condition, select)
            }

            promise
                .then(function(images) {
                    sails.log.info("image count returned:", images.length)
                    var imageLinks = images.map(function(image) {
                        var url = 'mediaServer/image/' + image.id;

                        if (details) {
                            sails.log.info("serve detilas", details);
                            return {
                                link: url + '?size=origin',
                                thumb: url,
                                tag: image.tag,
                                category: image.category,
                                width: image.width,
                                height: image.height,
                                format: image.contentType,
                                createdAt: image.createdAt,
                                status: image.status,
                                source: image.source,
                                thirdPartyId: image.thirdPartyId,
                                theme: image.theme,
                                size: image.fileSize
                            }
                        } else {
                            return {
                                link: url + '?size=origin',
                                thumb: url
                            };
                        }
                    });
                    var ret = {
                        totalCount: count,
                        pageSize: pageSize,
                        pages: Math.ceil(count / pageSize),
                        currentPage: pageNumber,
                        links: imageLinks
                    }
                    res.ok(ret);
                })
        })
        .catch(function(err) {
            sails.log.error("search failed, details, category, pageNumber, pageSize,orderBy",
                details,
                category,
                pageNumber,
                pageSize,
                orderBy
            )
        })
};

function serveImage(req, res) {

    // by default, serve thumb images
    var imageId = req.param('imageId');
    var width = req.param('width');
    var height = req.param('height');
    var imageSize = req.param('size');

    Media.findOne({ id: imageId }).populateAll()
        .then(function(file) {

                if (!file) {
                    sails.log.info('not found ');
                    return res.notFound();
                }

                var base64Image = file.data;
                var originFile = file.mediaFile;

                if (imageSize === 'origin' || base64Image == null) {
                    if (originFile)
                        base64Image = originFile.data;

                }

                //  else {
                //    width = parseInt(width);
                //    height = parseInt(height);
                //    //-------------------------------------
                //    sails.log.info("resize image");
                //    var img = new Buffer(originFile.data);
                //    jimp.read(img, originFile.contentType)
                //      .then(function (image) {
                //        var originSizeWidth = image.bitmap.width;
                //        var originSizeHeight = image.bitmap.height;
                //        var targetWidth = width;
                //        var targetHeight = ( width / originSizeWidth) * originSizeHeight;
                //        base64Image = image.resize(targetWidth, targetHeight);

                //        var img = new Buffer(base64Image, 'base64');

                //        res.writeHead(200,{
                //         "Content-Type": file.contentType,
                //         "Content-Length": img.length
                //        });

                //        res.end(img);

                //        return
                //     });
                //    //-------------------------------------
                //  }

                var img = new Buffer(base64Image, 'base64');

                res.writeHead(200, {
                    "Content-Type": file.contentType,
                    "Content-Length": img.length
                });

                res.end(img);
            },
            function(err) {
                res.serverError(err);
            })
};

function playVideo(req, res) {

    var token = req.params.token;
    var range = req.headers.range;
    var positions = range.replace(/bytes=/, "").split("-");
    var start = parseInt(positions[0], 10);

    var data = cache[token];
    data = null;
    if (data == null) {
        Video.findOne({ urltoken: token }).then(function(video) {
            var mediaPath = video.path;

            console.log(video);
            console.log("root=" + root);
            var mediaPath = path.join(root, mediaPath);
            console.log(mediaPath);

            //  fs.readFile(mediaPath, function (err, data) {
            fs.stat(mediaPath, function(err, stats) {
                if (err) {
                    res.statusCode = 500;
                    res.writeHead(500, err);
                    res.end(err);
                }

                var total = stats.size;
                var end = positions[1] ? parseInt(positions[1], 10) : total - 1;
                var chunksize = (end - start) + 1;

                res.writeHead(206, {
                    "Content-Range": "bytes " + start + "-" + end + "/" + total,
                    "Accept-Ranges": "bytes",
                    "Content-Length": chunksize,
                    "Content-Type": "video/mp4"
                });

                var stream = fs.createReadStream(mediaPath, { start: start, end: end })
                    .on("open", function() {
                        stream.pipe(res);
                    }).on("error", function(err) {
                        res.end(err);
                    });

                //cache[token] = data;
                //res.writeHead(200, {'Content-Length': data.length, 'Content-Type': 'video/mp4'});
                //res.end(data);
            });
        })
    } else {
        res.writeHead(200, { 'Content-Length': data.length, 'Content-Type': 'video/mp4' });
        res.end(data);
    }
};
