var Q = require("q");
var ObjectID = require('mongodb').ObjectID;

var getBookById = function(bookId) {
    var defer = Q.defer();
    Book.findOne({ id: bookId }).populateAll()
        .then(
            function(book) {
                if(!book){
                 return defer.reject(null);
                }

                // sort pages by index
                book.pages.sort(function(a, b) {
                    return a.index - b.index;
                })
                defer.resolve(book);
            },
            function(err) {
                defer.reject(err);
            }
        );
    return defer.promise;
};

var createBook = function(bookData) {
    var deferred = Q.defer();
    Book.create({
            title: bookData.title,
            titleFont: bookData.titleFont,
            titleColor: bookData.titleColor,
            attribute: bookData.attribute,
            attributeFont: bookData.attributeFont,
            attributeColor: bookData.attributeColor,
            backgroundColor: bookData.backgroundColor,
            frontCover: bookData.frontCover,
            backCover: bookData.backCover,
            dedicatedPage: bookData.dedicatedPage,
            frontCoverImageIndex: bookData.frontCoverImageIndex,
            backCoverImageIndex: bookData.backCoverImageIndex,
            dedication: bookData.dedication,
            dedicationFont: bookData.dedicationFont,
            dedicationColor: bookData.dedicationColor,
            desc: bookData.desc,
            rate: bookData.rate,
            //  data : bookData.data,
            openToAll: bookData.openToAll,
            pageCount: bookData.totalPage,
            author: bookData.author
        })
        .then(function(book) {
            // for each page, create a page record in database, and return the page id back
            var promises = bookData.pages.map(function(page) {
                page.book = book.id;
                return BookPage.create(page);
                //var canvas = fabric.canvas;
                //var fabric = require('fabric').fabric;
            });

            return Q.allSettled(promises)
                .then(function(data) {
                    var pages = data.map(function(ele) {
                        delete ele.value.active;
                        delete ele.value.createdAt;
                        delete ele.value.updatedAt;
                        delete ele.value.imageUrl;
                        return ele.value;
                    });

                    pages.sort(function(a, b) {
                        return a.index - b.index;
                    });

                    //book.pages = pages;   // this doesn't work
                    var bookClone = JSON.parse(JSON.stringify(book)); // this works
                    //var bookClone = Object.create(book);
                    //var bookClone = Object.assign({},book);
                    bookClone.pages = pages;
                    return deferred.resolve(bookClone);
                });
        })
        .catch(function(err) {
            sails.log.error("Create book failed, error:", err);
            return deferred.reject(err);
        })

    return deferred.promise;
};

var updateBookById = function(bookId, dataToUpdate) {
    var defer = Q.defer();
    Book.findOne({ id: bookId })
        .then(function(book) {
            if (!book) {
                sails.log.error("Error:", err);
                return defer.reject("No such book");
            }
            book.title = dataToUpdate.title;
            book.titleFont = dataToUpdate.titleFont;
            book.titleColor = dataToUpdate.titleColor;
            book.attribute = dataToUpdate.attribute;
            book.attributeFont = dataToUpdate.attributeFont;
            book.attributeColor = dataToUpdate.attributeColor;
            book.backgroundColor = dataToUpdate.backgroundColor;
            book.frontCoverWidth = dataToUpdate.frontCoverWidth;
            book.frontCoverHeight = dataToUpdate.frontCoverHeight;
            book.frontCover = dataToUpdate.frontCover;
            book.backCover = dataToUpdate.backCover;
            book.dedicatedPage = dataToUpdate.dedicatedPage;

            book.frontCoverImageIndex = dataToUpdate.frontCoverImageIndex;
            book.backColorImageIndex = dataToUpdate.backColorImageIndex;
            book.dedication = dataToUpdate.dedication;
            book.dedicationFont = dataToUpdate.dedicationFont;
            book.dedicationColor = dataToUpdate.dedicationColor;
            book.desc = dataToUpdate.desc;
            book.rate = dataToUpdate.rate;
            // book.data = dataToUpdate.data;
            book.openToAll = dataToUpdate.openToAll;
            book.pageCount = dataToUpdate.totalPage;

            book.save()
                .then(function() {
                    sails.log.info("update:save finished ...");

                    var promises = dataToUpdate.pages.map(function(page) {
                        //page.book = book.id;
                        //delete page.id;
                        var p = {}
                        p.imageData = page.imageData;
                        p.imageUrl = page.imageUrl;
                        p.width = page.width;
                        p.height = page.height;
                        p.canvasId = page.canvasId;
                        p.index = page.index;
                        p.show = page.show;
                        p.active = page.active;
                        p.book = book.id;
                        return BookPage.create(p);
                    });

                    BookPage.destroy({ book: new ObjectID(bookId) }) // delete existing first;
                        .then(function() {
                            // return Q.resolve(book);
                            return Q.allSettled(promises)
                        })
                        .then(function(data) {
                            sails.log.info(" in update, saving pages finished ...");
                            var pages = data.map(function(ele) {
                                delete ele.value.active;
                                delete ele.value.createdAt;
                                delete ele.value.updatedAt;
                                delete ele.value.imageUrl;
                                return ele.value;
                            });
                            pages.sort(function(a, b) {
                                return a.index - b.index;
                            });

                            var bookClone = JSON.parse(JSON.stringify(book));
                            bookClone.pages = pages;
                            return defer.resolve(bookClone);
                        })
                })
                .catch(function(err) {
                    sails.log.error("Error:", err);
                    return defer.reject(err);
                })
        })
    return defer.promise;
};

var getBooks = function(condition, count, orderByRate) {
  var promise;

  if(!count && !orderByRate){
    return Book.find(condition).populate("author").sort({createdAt: -1});
  } else if(orderByRate === true) {
    promise = Book.find(condition).populate("author").sort({rate: -1}).limit(count);
  } else {
    ////  select book randomly
     return Book.find(condition, {select: ['id']})
      .then(function (ids) {
        var totalCount = ids.length;
        var ids = _.shuffle(ids).splice(0, count).map(function (id) {
          return id.id;
        });

        condition = {
          id: ids
        };

        return promise = Book.find(condition).populate("author").limit(count);
      })
  }

  return promise;
};

var deleteBookById = function(bookId) {
    return Book.destroy({ id: bookId })
};

var deleteUserBooks = function(userId) {
    return deleteBook({ author: userId });
};

function deleteBook(condition) {
    var defer = Q.defer();
    Book.destroy(condition).exec(function(err) {
        if (err) {
            return defer.reject(500);
        }

        return defer.resolve();
    });

    return defer.promise;
}

module.exports = {
    getBooks: getBooks,
    createBook: createBook,
    updateBookById: updateBookById,
    getBookById: getBookById,
    deleteBookById: deleteBookById,
    deleteUserBooks: deleteUserBooks
}
