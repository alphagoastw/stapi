var fs = require('fs');
var q = require('q');

module.exports = {
    saveToMediaCollection: saveToMediaCollection,
    deleteMedia: deleteImage,
    updateMedia: updateMedia,
    updateMediaById: updateMediaById,
    batchDeleteMedia: batchDeleteMedia
};

function saveToMediaCollection(filePath, tag, owner, fileSize, width, height, category, contentType, mediaFileId,
    thirdPartyId, hitCount, source, theme, scope, shared) {
    var defer = q.defer();
    var fileParam = {};

    fileParam.tag = tag;
    fileParam.owner = owner;
    fileParam.width = width;
    fileParam.height = height;
    fileParam.fileSize = fileSize;
    fileParam.category = category;
    fileParam.contentType = contentType;
    fileParam.mediaFile = mediaFileId;
    fileParam.thirdPartyId = thirdPartyId;
    fileParam.hitCount = hitCount ? hitCount : 0;
    fileParam.source = source ? source : 'storyToaster';
    fileParam.status = "enabled";
    fileParam.theme = theme;
    fileParam.scope = scope || 'global';
    fileParam.shared = shared || true;

    if (category === 'photo') {
        fileParam.scope = 'personal';
        fileParam.shared = false;
    }

    /// get the thumb media data
    getMediaFileData(filePath, mediaFileId)
        .then(function(data) {
            fileParam.data = data;
            Media.create(fileParam, function(err, mediaCreated) {
                if (err) {
                    sails.log.error("Media.create failed. Error=", err);
                    return defer.reject(err);
                }
                var id = mediaCreated.id;
                // update User.profile field
                User.update({ id: owner }, { icon: id }, function(err, updatedUser) {
                    if (err) {
                        sails.log.error("error to find user id:", id);
                        return err;
                    }
                    sails.log.info("update user.profile finished ");
                    return defer.resolve(id);
                });
            });
        });
    return defer.promise;
}

function getMediaFileData(filePath, mediaFileId) {
    var defer = q.defer();
    if (!mediaFileId) {
        defer.resolve(null);
    }

    fs.readFile(filePath, 'binary', function(err, data) {
        if (err) {
            return defer.resolve(null);
        }

        var base64Image = new Buffer(data, 'binary').toString('base64');
        return defer.resolve(base64Image);
    });

    return defer.promise;
}

function batchDeleteMedia(ids) {
    var defer = q.defer();
    var promises = ids.map(function(id) {
        return deleteImage(id);
    })

    q.all(promises)
        .then(function(res) {
            sails.log.info("Batch delete Image finished successfully. ");
            return defer.resolve(res);
        })
        .catch(function(err) {
            sails.log.error("Batch delete Image finished failed:", err);
            return defer.reject(err);
        });

    return defer.promise;
}

function deleteImage(id) {
    var defer = q.defer();

    Media.destroy({ id: id }).exec(function(err, media) {

        if (err) {
            return defer.reject(err);
        }

        if (!media || media.length < 1) {
            return defer.resolve(null);
        }

        MediaFile.destroy({ id: media[0].mediaFile }).exec(function(err, mediaFile) {
            // do care mediaFile deletion fails or not
        });

        return defer.resolve(media.length);
    });

    return defer.promise;
}

// batch update image information 
//example data 
// var data =  
//     [{
//       id : "5831ec5851e26b6415db2236",
//       source : "storyToaster",
//       status : "enabled",
//       tag : "wewrewr"  
//    }] ;

function updateMedia(data) {
    var defer = q.defer();
    var orginData = data;
    var promises = data.map(function(ele) {
        var id = ele.id;
        delete ele.id;
        return Media.update(id, ele);
    })

    q.all(promises)
        .then(function(res) {
            sails.log.info("Update Image finished successfully. ");
            var data = res.map(function(res) {
                delete res[0].data;
                return res[0];
            })
            return defer.resolve(data);
        })
        .catch(function(err) {
            sails.log.error("Update Image finished failed:", err);
            return defer.reject(err);
        });

    return defer.promise;
}

function updateMediaById(mediaId, data) {
    return Media.update(mediaId, data);
}