var jwt = require("jwt-simple");
var sessionTokenHelper = require("../services/sessionTokenHelper.js");
var ObjectID = require('mongodb').ObjectID;

module.exports = function(req, res, next){

    if(isExcluded(req.url)) {
        return  next(); 
    } 

    var token = req.headers.authorization;
    var payload = sessionTokenHelper.getPayloadFromSessionToken(token);
    var headerUserId = req.headers.uid;
    var userid = payload.userid;  
    var uid = req.param("uid"); 
    req.session.userid = userid;  
    
    if (!userid ) {
        return res.status(401).send({
            message: "Authentication failed,no userId - checkWritePermission"
        });
    }  else {

      if(req.method === 'GET') return next();   
      User.findOne({_id:new ObjectID(userid)}).
        exec(function (err, userFound) {   
           
         sails.log.info(userFound);

          if (err){ 
              sails.log.error("error:", err);
              return res.status(401).send({
                    message: "You are not a valid user. - checkWritePermission"
              });
          }
          else { 
            if(userFound && userFound.userType=="admin"){
                req.session.role = 'admin';
                sails.log.info("-------- admin user ", userFound.userName);   
                next();
            } else {
               // use can only write to his/her own course/module/videos
               //if(headerUserId != userid){
              sails.log.info("-------- check permission ", userFound.userName);   

              if(!uid || userId !== uid )
              { 
                sails.log.info("No permisssion ", userFound.userName);  
                //   return res.forbidden('Access denied.  checkWritePermission');
                return res.status(401).send({
                    message: "You are not a valid user. - checkWritePermission"
                });
              }; 
              next();
            } 
          }
        })
    }
};


function isExcluded(url){
   var urls = [
    '/auth/resetPassword' 
   ]

   return urls.indexOf(url) > -1; 
}
