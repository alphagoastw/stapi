var jwt = require("jwt-simple");
var sessionTokenHelper = require("../services/sessionTokenHelper.js");
var urlQuery = require('url');

getSessionToken = function(req){
  var sessionToken ='';
  if (!req.headers || !req.headers.authorization) {
    // check query string
    var query = urlQuery.parse(req.url,true).query;
    sessionToken = query.sessionToken;
  }
  else{
    var token = req.headers.authorization;
    sessionToken = token;
  }

  if(!sessionToken){
    sessionToken = req.headers['auth-token'];
  }
  if(sessionToken =='undefined') sessionToken = null;
  return sessionToken ;
};

module.exports = function(req, res, next) {
   
  if(isExcluded(req.url)) {
      return  next(); 
  } 

  var token = getSessionToken(req);
  if (token) {

    var payload = sessionTokenHelper.getPayloadFromSessionToken(token);

    if (!payload ||  !payload.userid) {
      return res.status(401).send({
        message: "Authentication failed (0)- jwtAuth"
      });
    }
    else {
      req.session.userid = payload.userid;
      req.headers.uid = payload.userid;
      next();
    }
  } else {
    return res.status(401).send({
      message: "Authentication failed (1)- jwtAuth"
    });
  }
};

function isExcluded(url){
   var urls = [
    '/auth/resetPassword' 
   ]

   return urls.indexOf(url) > -1; 
}
