var formidable = require('formidable');

var videoOptions = require("./settings/videoFileSettings.js").options;
var util = require('util');
var fs = require('fs');
var path = require("path");
var root = require('app-root-path') + "";
var jimp = require('jimp');
var q = require('q');

var extend = require('util')._extend;
var randomstring = require("randomstring");
var Hashids = require("hashids");

var flash = require('connect-flash');
var inspect = require('util').inspect;
var tokenHelper = require("../services/tokenHelper.js");
var options = require("./settings/jqueryFileSetting.js").options;

var fs = require('fs');
var path = require("path")
var root = require('app-root-path') + "";

var saveThumbFile = function(origFilePath, targetThumbFile, imageType) {
    var defer = q.defer();

    // for background,  the width/height=4:3  (68:51)
    // for props and profileIcon, the width/height=68 :x
    // for text,  the width/height=150 :x

    var originSize = {};

    jimp.read(origFilePath)
        .then(function(image) {

            // if the image is a font image,  or the image is from a design page,  do not create thumb image
            if (imageType === 'font' || imageType === 'page') {
                var mediaFileInfo = {
                    originWidth: image.bitmap.width,
                    originHeight: image.bitmap.height,
                    originFileSize: image.bitmap.length
                };

                defer.resolve(mediaFileInfo);
                return defer.promise;
            }

            originSize.width = image.bitmap.width;
            originSize.height = image.bitmap.height;

            var targetWidth = 68;
            var targetHeight = 51;

            var imageBuff;
            if (imageType === 'background' || imageType === 'bigText') {
                imageBuff = image.resize(targetWidth, targetHeight)
            } else if (imageType === 'props') {
                imageBuff = image.resize(68, jimp.AUTO)
            } else {
                imageBuff = image.resize(150, jimp.AUTO)
            }

            imageBuff.write(targetThumbFile, function(err, data) {
                if (err) {
                    sails.log.error(err);
                    return defer.reject(err);
                };
                var mediaFileInfo = {
                    originWidth: originSize.width,
                    originHeight: originSize.height,
                    originFileSize: image.bitmap.length,
                    thumbWidth: imageBuff.bitmap.width,
                    thumbHeight: imageBuff.bitmap.height,
                    thumbFileSize: imageBuff.bitmap.length
                };

                return defer.resolve(mediaFileInfo); // always return succeed
            });
        })
        .catch(function(err) {
            return defer.reject(err);
        });

    return defer.promise;
};

var uploadImage = function(req, res) {

    var uploadFile = req.file('uploadFile');
    var uploadOptions = {
        maxBytes: 100000000 // 100 M
    };

    var thirdPartyId = req.body.imageName;
    var tag = req.body.tag;
    if (!tag && req.body.data) {
        var bodyData = JSON.parse(req.body.data);
        if (bodyData) {
            tag = bodyData.tag;
            thirdPartyId = bodyData.imageName;
        }
    }

    var theme = req.body.theme;
    var source = req.body.source || '';
    var scope = req.body.scope || 'global';
    var shared = req.body.shared || true;

    if (thirdPartyId)
        thirdPartyId = path.basename(thirdPartyId, ".png");

    if (req.session.userid == null) {
        return res.status(301).send("Not logged");
    }

    var owner = req.session.userid;
    var imageType = req.param('cat') || 'props';
    uploadFile.upload(uploadOptions, function onUploadComplete(err, files) {
        if (!files) {
            sails.log.error("upload failed...error", err);
            res.status(500).send(err);
        }
        sails.log.info('------ on upload finished ---------------------------------------');

        var category = imageType;
        var contentType = files[0].type;
        var originFilePath = files[0].fd;
        var filePath = path.parse(originFilePath);
        var targetThumbFile = path.join(filePath.dir, filePath.name + "_thumb" + filePath.ext);
        var fileSize = files[0].size;

        saveThumbFile(originFilePath, targetThumbFile, imageType)
            .then(function(thumbMediaInfo) {

                var originWidth = thumbMediaInfo.originWidth;
                var originHeight = thumbMediaInfo.originHeight;
                sails.log.info("================== save ThumbFile finished ====================");
                mediaFileRepository.saveToMediaFileCollection(originFilePath, fileSize, originWidth, originHeight, category, contentType)
                    .then(function(mediaFileObj) {
                        if (!mediaFileObj) {
                            sails.log.error("saving saveToMediaFileCollection failed, filename: ", originFilePath);
                            res.status(500).send(originFilePath);
                        }

                        return mediaRepository.saveToMediaCollection(targetThumbFile, tag, owner,
                            fileSize, originWidth, originHeight, category, contentType,
                            mediaFileObj.id, thirdPartyId, 0, source, theme, scope, shared);
                    })
                    .then(function(mediaId) {
                            sails.log.info("created=", mediaId);
                            res.status(201).send(mediaId);
                        },
                        function(err) {
                            sails.log.error('upload file fails, error:', err);
                            return res.serverError(err);
                        })
                    .done(function() {
                        fs.unlink(originFilePath);
                        fs.unlink(targetThumbFile);
                    })
            })
            .catch(function(err) {
                fs.unlink(originFilePath);
                res.status(400).send("Not supported File format");
            })

        // save originFile
    })
}


module.exports = {
    upload: uploadImage
};