/**
 * Created by bwang on 09/09/2015.
 */
/**
 * RootController
 *
 * @description :: Server-side logic for managing roots
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var package = require('../../package.json');
var LogstashUDP = require('winston-logstash-udp').LogstashUDP;

module.exports = {

  create:function(req,res){
    return res.status(401).send({'status':'POST is not allowed'});
  },

  update:function(req,res){
    return res.status(401).send({'status':'PUT is not allowed'});
  },

  destroy:function(req,res){
    return res.status(401).send({'status':'DELETE is not allowed'});
  },

  findOne:function(req,res){
    return res.status(404).send({'status':'Path is not supported'});
  },

  findVersions:function(req,res) {
    var version = {};
    version.version = '1.0.0';
    version.build = package.version;
    sails.log.info("this is info test from version ");
    sails.log.debug("this is debug test from version ");
    sails.log.error("this is error test from version ");
    sails.log.info('NODE_ENV = ',process.env.NODE_ENV);

    version.env  = process.env.NODE_ENV;
    res.ok(version);
  }
};


