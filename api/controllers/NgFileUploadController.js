/**
 * Created by Administrator on 24/05/2015.
 */
var formidable = require('formidable');

var videoOptions = require("./settings/videoFileSettings.js").options;
var util = require('util');
var fs = require('fs');
var path = require("path");
var root = require('app-root-path') + "";
var Ffmpeg = require('fluent-ffmpeg');

var randomstring = require("randomstring");
var courseidKey = "sec for construct course id";
var sectionKey = "sec for construct couse section id";
var Hashids = require("hashids"),
  courseHashids = new Hashids(courseidKey),
  sectionHashids = new Hashids(sectionKey);
var urlQuery = require('url');
var flash = require('connect-flash');

var Busboy = require("busboy");
var inspect = require('util').inspect; ;

var tokenHelper = require("../services/tokenHelper.js");

var createUploader2 = function (req, dir) {
  dir = dir.replace(/\\/g, "/");
  options.uploadDir = dir;
  options.tmpDir = dir;
  options.public = dir;
  var uploader = require('blueimp-file-upload-expressjs')(options);
  return uploader;
};

var options = require("./settings/jqueryFileSetting.js").options;

var fs = require('fs');
var path = require("path")
var root = require('app-root-path') + "";
var Ffmpeg = require('fluent-ffmpeg');

var createFolder = function (dir) {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
};

var createMediaFolder = function (tutorId, courseId) {
  var tutorFolder = path.join(root, "media/tutors/", tutorId + "");
  var tutorCoursesFolder = path.join(tutorFolder, "/courses/");
  var courseFolder = path.join(tutorCoursesFolder, courseId + "");
  var originFolder = path.join(courseFolder, "/origin");
  createFolder(tutorFolder);
  createFolder(tutorCoursesFolder);
  createFolder(courseFolder);
  createFolder(originFolder);

  return originFolder;
};

var processVideoUploading = function (req, res,mediaFormData, moduleId, courseId, tutorId, videoFilePath) {
    //Ffmpeg.ffprobe(videoFilePath, function (err, metadata) {
    //var duration = 0;
    //metadata.streams.forEach(function (mediaInfo) {
    //  if (mediaInfo.codec_type == "video") {
    //    duration = mediaInfo.duration;
    //    return;
    //  }
    //});
    //if(metadata) {
    //  duration = metadata.format.duration;
    //}
    console.log(mediaFormData);
    var relativeVideoPath = videoFilePath.replace(root,"");
    Video.create(
      {
        name: mediaFormData.videoname || "  ",
        tutor: {id:tutorId},
        course:{id:courseId},
        module:{id:moduleId},
        size: mediaFormData.filesize,
        format: mediaFormData.filetype,
        duration: mediaFormData.duration,
        path: relativeVideoPath
      },
      function (err, data) {
        if (!!err) {
          console.log('error in upload video' + err);
          console.log(JSON.stringify(err));
        }
        // update duration time in course, and module

        var ids = [tutorId, courseId, moduleId, data.id];
        var idToken = courseHashids.encode(ids);
        mediaFormData.urltoken = idToken;

        data.urltoken = mediaFormData.urltoken;
        data.save(function(err){
          if(err){}
          else
            res.send(data);
        });
      });
  //});
};

var uploadVideo = function(req,res){

    console.log('------ data');
    console.log(req.body.data);

    var uploadFile = req.file('uploadFile');
    var uploadOptions = {
      // dirname: 'D:/Khufu_I/Videos',
      maxBytes:100000000
    };
    uploadFile.upload(uploadOptions,function onUploadComplete(err, files) {
      //	Files will be uploaded to .tmp/uploads
      if (err) return res.serverError(err);

      var tutorId = parseInt( req.params.tutorId);
      var courseId = parseInt(req.params.courseId);
      var moduleId = parseInt(req.params.moduleId);

      var folder = createMediaFolder(tutorId, courseId);
      var formObj = {};
      formObj.filesize = files[0].size;
      formObj.filename = files[0].filename;
      formObj.filetype = files[0].type;
      console.log(req.body.data);
      if(req.body.data){
        formObj.videoname = JSON.parse(req.body.data).videoname;
        formObj.duration = JSON.parse(req.body.data).duration;
      }
      else
        formObj.videoname = '';

      var filepath = formObj.path = files[0].fd;
      console.log(folder);
      console.log(filepath);

      var baseFileName = path.basename(filepath);
      var extname = path.extname(filepath);

      var targetfile = path.join(folder,baseFileName);
      console.log(targetfile);

      fs.createReadStream(filepath).pipe(fs.createWriteStream(targetfile));

      if(formObj.filetype == 'video/mp4' || formObj.filetype == 'video/webm'){
        processVideoUploading(req,res,formObj,moduleId, courseId, tutorId, targetfile);
      }else{
        res.json({status: 401,Error:"not supported"});
      }
    })
};

var deleteVideo = function(req, res){
  if(req.method != 'DELETE')
    return res.json({'status':'GET not allowed'});

  var urlToken = req.params.urlToken;
  var tutorId = parseInt(req.params.tutorId);
  if( !urlToken || !tutorId ){
    return res.json({status:400, Error:'Error operation'});
  };

  var currentUserId = req.session.userid;
  //var ids = [tutorId, courseId, moduleId, data.id];
  var ids = courseHashids.decode(urlToken);
  if(ids[0] != tutorId || currentUserId != tutorId){
    return res.json({status:401, Error:'Unauthorized operation'});
  };

  //Video.destroy({courseId:ids[1], moduleId:ids[2], urltoken:urlToken}).exec(function (err, video) {
  Video.findOne({urltoken:urlToken}).then(function(video){
    //if(err) {
    //  return res.json({status: 404, Error: 'Not found'});
    //}
   video.destroy().then(function(data){
     console.log(data);
     Module.findOne({id:video.module}).then(function(module){

        module.duration -= video.duration;
        module.save();
        Course.findOne({id:module.course}).then(function(course){
          course.duration -= video.duration;
          course.save();
        })
      });
      return res.json({status:200, Video:video});
   });
  });
 };

module.exports = {
  upload: uploadVideo,
  deleteVideo : deleteVideo
};


