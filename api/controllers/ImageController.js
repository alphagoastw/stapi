var q = require('q'); 

module.exports = { 
  deleteImageById : deleteImageById,
 
  deleteAllImage: deleteAllImage, 
  batchDelete: batchDelete, 
  deleteUserImage:deleteUserImage,

  updateImageById: updateImageById,
  batchUpdate : batchUpdate,
  updateUserImageById: updateUserImageById,

  getImagesMetaData : getImagesMetaData
};
  
// POST /media/meta/images 
function getImagesMetaData(req, res){
  // data :[id1, id2,...]
  // this function can only be used by admin
  console.log(req.session.role); 

  var cat = req.param('cat'); 
  var ids = req.body; 

  var orConditions = ids.map(function(i){
      return {
        id:i
      };
  });
 
  Media.find(
      {
         select:['id','tag','status', 'source','width','height','fileSize','owner'],
         where:{category:cat, or:orConditions}
      }
    )
   .then(function(ele){
     return res.status(200).send(ele);
   })
   .catch(function(err){
     sails.log.error("find media failed.  condition:", orConditions, " category:",cat);
     return res.serverError(err);
   })
 
} 

// , 'delete /media/:uid/images/:imageId'
function updateUserImageById(req,res){
   
    var userId = req.param('uid'); 
    if(req.session.userid !== userId) {
      res.forbidden('Access denied.');
    } 
    var mediaId = req.param("imageId"); 
    if(!mediaId){
      return res.badRequest("Not a valid media id");
    }

    mediaRepository.updateMediaById(mediaId, req.body)
      .then(function(data){
        return res.ok(data);
      })
      .catch(function(err){
        return res.serverError(err);
      })
}

//   , 'PUT /media-batch/images
function batchUpdate(req, res){  
     // this function can only be used by admin
     if(req.session.role !="admin"){
       return res.forbidden("You don't have permission to do media batch update."); 
     }
     sails.log.info("data to update:",req.body);

      mediaRepository.updateMedia(req.body)
      .then(function(data){
         return res.ok(data);
      })
      .catch(function(err){
         return res.serverError(err);
      });  
}

// 'PUT /media/images/:imageId'  used by admin
function updateImageById(req, res) { 

    if(req.session.role !="admin"){
       return res.forbidden("You don't have permission to change other people's media"); 
     } 

     var imageId = req.param("imageId"); 
     if(!imageId){
        return res.badRequest("Not a valid media Id");
     }

     var data = {};

     data.tag = req.body.tag;
     data.status = req.body.status;
     data.source = req.body.source; 

     mediaRepository.updateMediaById(imageId, data)
     .then(function(ret){
       return res.ok(ret);
     })
     .catch(function(err){
       sails.log.error("Update media failed. param userId:",uid, " sessionUid:", sessionUid," data:", req.body);
     }).done(); 
}

//   , 'delete /media/:uid/images/:imageId'
function deleteUserImage(req,res) {
   var userId = req.param('uid'); 
    if(req.session.userid !== userId) {
      return res.forbidden('Access denied.');
    }
    
    var imageId = req.param("imageId");
    if(!imageId){
      return res.badRequest("Not valid image id");
    }

    mediaRepository.deleteMedia(imageId)
    .then(
    function (media) {
      if(!media){
        return res.notFound();
      }
      return res.ok(media.length);
    },
   function(){
     res.notFound();
   })
    .catch(function (err) {
      return res.serverError(err);
    }) 
}

function batchDelete(req,res){
  if(req.session.role !="admin"){
       return res.forbidden("You don't have permission to do batch update."); 
  } 

  var ids = req.body; 
  mediaRepository.batchDeleteMedia(ids)
  .then(function(d){
    return res.ok(d);
  })
  .catch(function(err){
    sails.log.error("Batch delete failed, ids:", ids,  " error ", err);
    return res.badRequest(err);
  })

  // todo 
}

function deleteImageById(req, res) {

  if (req.method != 'DELETE')
    return res.json({'status': 'GET not allowed'});
  sails.log.info("req.session.role=", req.session.role);
  if(req.session.role !="admin"){
       return res.forbidden("You don't have permission"); 
  } 

  var imageId = req.param('imageId');

  mediaRepository.deleteMedia(imageId)
    .then(
    function (media) {
      if(!media){
        return res.notFound();
      }
      return res.ok(media.length);
    },
   function(){
     res.notFound();
   })
    .catch(function (err) {
      return res.serverError(err);
    })
};

function deleteAllImage(req,res){
  Image.destroy().exec(function (err, images) {
    if (err) {
      return res.json({status: 404, Error: 'Not found one to delete'});
    }
    images.forEach(function(image){
      //delete fullsize image
      var fullSizeFilePath = path.join(root,'media','images','fullsize',image.category, image.path);

      //delete thumb image
      var thumbFilePath = path.join(root,'media','images','thumb',image.category, image.path);
      fs.unlink(thumbFilePath);
    });
    return res.json('all deleted');
  });
};


