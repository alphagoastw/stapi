/**
 * CourseSection.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    attributes: {

        bookId: {
            type: 'integer',
            unique: true,
            autoIncrement: true,
            columnName: 'bookId'
        },

        title: {
            type: "string",
            required: true,
            minLength: 1,
            maxLength: 100
        },

        titleFont: {
            type: "string"
        },

        titleColor: {
            type: "string"
        },

        attribute: {
            type: 'string'
        },

        attributeFont: {
            type: 'string'
        },

        attributeColor: {
            type: 'string'
        },

        backgroundColor: {
            type: 'string'
        },

        desc: {
            type: "string"
        },

        fontCoverImageIndex: {
            type: "integer"
        },

        backCoverImageIndex: {
            type: "integer"
        },

        dedication: {
            type: "string"
        },

        dedicationColor: {
            type: 'string'
        },

        dedicationFont: {
            type: 'string'
        },

        pageCount: {
            type: "integer"
        },

        author: {
            model: "User"
        },

        rate: {
            type: "integer",
            defaultsTo: 0
        },

        openToAll: {
            type: "boolean",
            defaultsTo: true
        },

        data: {
            type: "string"
        },

        pages: {
            collection: "BookPage",
            via: "book"
        }
    }
};