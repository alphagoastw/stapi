/**
* Product.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    productId:{
      type:'integer',
      unique: true,
      autoIncrement:true,
      columnName:'productId'
    },

    productType:{
      type:"string",
      enum: ["Book", "Photo"],
      defaultsTo: "Book"
    },

    desc:{
      type: "type"
    }
  },
  seedData: []
}
