var tokenHelper = require("../services/tokenHelper.js");
var tools = require("../services/common/tools.js");

module.exports = {

    attributes: {

        mediaId: {
            type: 'integer',
            unique: true,
            autoIncrement: true,
            columnName: 'mediaId'
        },

        fileName: {
            type: "string"
        },

        fileSize: {
            type: "integer"
        },

        category: {
            type: "string", // {'background','props','text'}
            enum: ["background", "props", "text", "font", "profileIcon", "photo", "page", "bigText"], //text：speach bubble， bigText：big text editor
            defaultsTo: "background"
        },

        contentType: {
            type: "string"
        },

        source: {
            type: "string",
            defaultsTo: "storyToaster"
        },

        status: {
            enum: ["enabled", "disabled"],
            defaultsTo: "enabled"
        },

        name: {
            type: "string"
        },

        thirdPartyId: {
            type: "string"
        },

        theme: {
            type: "string"
        },

        width: {
            type: "integer"
        },

        height: {
            type: "integer"
        },

        tag: {
            type: "string",
            required: false
        },

        size: {
            type: "integer"
        },

        owner: {
            model: "User",
            required: "true"
        },

        data: {
            type: "string"
        },

        mediaFile: {
            model: "MediaFile"
        },

        hitCount: {
            type: "integer"
        },

        scope: {
            type: "string",
            enum: ["global", "personal"],
            defaultsTo: "global"
        },

        shared: {
            type: "boolean",
            defaultsTo: true
        }
    }
};