/**
 * Built-in Log Configuration
 * (sails.config.log)
 *
 * Configure the log level for your app, as well as the transport
 * (Underneath the covers, Sails uses Winston for logging, which
 * allows for some pretty neat custom transports/adapters for log messages)
 *
 * For more information on the Sails logger, check out:
 * http://sailsjs.org/#!/documentation/concepts/Logging
 */
var winston = require('winston');
var LogstashUDP = require('winston-logstash-udp').LogstashUDP;
var dailyRotateFile = require('winston-daily-rotate-file');

module.exports = {

  /***************************************************************************
  *                                                                          *
  * Valid `level` configs: i.e. the minimum log level to capture with        *
  * sails.log.*()                                                            *
  *                                                                          *
  * The order of precedence for log levels from lowest to highest is:        *
  * silly, verbose, info, debug, warn, error                                 *
  *                                                                          *
  * You may also set the level to "silent" to suppress all logs.             *
  *                                                                          *
  ***************************************************************************/

  level: 'silly',
  'log': {
    'colors': false,
    'custom': new (winston.Logger)({
      'transports': [
        new (winston.transports.Console)({
          level: 'info',
          colorize: true,
          timestamp: false,
          json: false
        }),
        new winston.transports.File({
          level: 'silly',
          colorize: false,
          timestamp: true,
          json: true,
          filename: './logs/out.log',
          maxsize: 5120000,
          maxFiles: 3,
          handleExceptions: true,
          humanReadableUnhandledException: true
        }),
        new (dailyRotateFile)({
          filename:'./logs/out.log',
          localTime:true,
          prepend:true
        }),

        new(LogstashUDP)({
          //port: "28777",
          //host:'LOGSTASH_SERVER'
          host:process.env.LOGSTASH_HOST,
          port:process.env.LOGSTASH_PORT,
          appName: 'StoryToasterAPI'
        })
      ]
    })
  }

};
