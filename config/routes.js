/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': {
    view: 'homepage'
  },

  '/upload-file': {
    view: 'uploadfile'  // view 'uploadfile' in views directory will loaded automatically
  },
  /**********************below is for local login *********************************/
  "/local-login": {
    view: 'login'
  },
  'GET /version' :'VersionController.findVersions',

  'get /files' : 'FileController.getfiles',
  'get /files/:fileId' : 'FileController.getFileById',
  'post /file/upload': "FileController.upload",
  'get /mediaServer/video/stream/:token': "MediaServerController.streamVideo"

  , 'get /mediaServer/image': "MediaServerController.getImages"
  , 'get /mediaServer/photo': "MediaServerController.getPersonalImages"
  , 'get /mediaServer/image/:imageId': "MediaServerController.serveImage"
  , 'get /mediaServer/page/:pageId': "MediaServerController.serveBookPageImage"
  , 'post /mediaServer/image/': "ImageUploadToDBController.upload"

  , 'delete /media/images/:imageId': "ImageController.deleteImageById"                // used by admin
  , 'POST /media-batch/images' : "ImageController.batchDelete"                 // used by admin

  , 'delete /media/:uid/images/:imageId' : "ImageController.deleteUserImage"     // used by user
  , 'get /media/:uid/images/:imageId' : "ImageController.getUserImageInfoById"   // get user's image information

  , 'PUT /media/images/:imageId': "ImageController.updateImageById"              // used by admin
  , 'PUT /media-batch/images': "ImageController.batchUpdate"                     // used by admin

  , 'PUT /media/:uid/images/:imageId' : "ImageController.updateUserImageById"    // used by user

  , 'delete /media/image-all': "ImageController.deleteAllImage"                  // used by admin

  // For media information (FOR GET)
  , 'POST /media/meta/images' : "ImageController.getImagesMetaData"

  , 'post /users/:uid/profile' :'UserController.updateUserProfile'
  , 'get /users/:uid/profile': 'UserController.getUserProfile'

  , 'get /auth/profile/:id': 'AuthController.getUserProfile'
  , 'get /auth/checkUserName/:username': 'AuthController.checkUsername'
  , 'get /auth/checkEmail/:email': 'AuthController.checkEmail'
  , 'get /registration/confirm': 'AuthController.registerConfirm'

  // Send forget password email, querystring email=? username =?
  , 'get /auth/forgetPassword': 'AuthController.forgetPassword'

  // parameter:  token, email,  password, passwordConfirm
  , 'post /auth/resetPassword': 'AuthController.resetPassword'
  , 'get /auth/token/:token' : 'AuthController.getTokenData'

  , 'post /auth/:uid/changePassword':'AuthController.changePassword'
  , 'post /auth/register': 'AuthController.register'
  , 'post /auth/login':'AuthController.login'
  , 'get  /auth/logout':"AuthController.local_logout"
  , 'post /auth/loginByUserNameOrEmail':'AuthController.loginByUserNameOrEmail'
  , 'post /auth/:uid/updateUserNameAndPassword':'AuthController.updateUserNameAndPassword'

  , 'get /test/auth': 'User.auth'

  ///************************************* book resource

  // mode=random or popular
   // random choose count number books,
   // or choose most popular count number books
   // or search by title

   // /books?count=?&mode=&title
  , 'get /books': 'BookController.getBooks'
  , 'get /books/:id': 'BookController.getBookById'

  //, 'get /books/:id/pages/:pageId': 'BookController.getBookPage'

  , 'post /books': 'BookController.createBook'
  , 'put /books/:id': 'BookController.updateBookById'
  , 'delete /books/:id': 'BookController.deleteBookById'
  , 'delete /users/:userId/books' : 'BookController.deleteUserBooks'
  , 'get /users/:uid/books': 'BookController.getUserBooks'

  ///************************************* book comments resource
  , 'get /books/:id/comments' : 'BookCommentController.getBookComments'
  , 'post /books/:id/comments' :'BookCommentController.createComment'
  , 'put /books/:bookId/comments/:commentId' : 'BookCommentController.updateComment'
  , 'delete /books/:bookId/comments/:commentId' : 'BookCommentController.deleteComment'

  ///************************************* book pages resource

  , 'get /books/:bookId/pages': 'BookPageController.getBookPages'
  , 'post /books/:bookId/pages': 'BookPageController.createBookPage'
  , 'get /books/:bookId/pages/:pageId':'BookPageController.getOneBookPage'
  , 'put /books/:bookId/pages/:pageId':'BookPageController.updateOneBookPage'
  , 'delete /books/:bookId/pages/:pageId':'BookPageController.deleteOneBookPage'

  /*****************************  to import *********************************/

  , 'get /import' : 'ImportController.import'
  , 'get /import-meta' : 'ImportController.importMeta'



  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
