module.exports.connections = {

  //localDiskDb: {
  //  adapter: 'sails-disk'
  //},

  localMongodbServer: {
    adapter: 'sails-mongo',
    //url:"mongodb://icetrain:icetrain@localhost,192.168.1.71,192.168.1.71:27018/stapi?replicaSet=rs0"
    url: "mongodb://localhost,192.168.1.71,192.168.1.71:27018,192.168.1.57/stapi?replicaSet=rs0&slaveOk=true&readPreference=secondaryPreferred",
    replSet: {
      servers: [{
        host: 'localhost',
        port: 27017
      },
        {
          host: '192.168.1.57',
          port: 27017
        },
        {
          host: '192.168.1.71',
          port: 27018
        }
      ]
    },
    slaveOk: true,
    readPreference: "secondaryPreferred"
    //host: 'localhost',
    //port: 27017,
    //username: 'icetrain', // storyToaster
    //password: 'icetrain', // ChfwtHNFQ5vBTv?x
    //database: 'stapi'
    ////database: 'storyToaster'
  },

  localSingleMongo: {
    adapter: 'sails-mongo',
    //url: "mongodb://localhost:27017/stapi"
    host: 'localhost',
    port: 27017,
    username: 'icetrain',
    password: 'icetrain',
    database: 'stapi'
  },

  mongodbLab: {
    adapter: 'sails-mongo',
    //mongodb://<dbuser>:<dbpassword>@ds159217.mlab.com:59217/story-toaster
    url: "mongodb://stapi:stapi@ds159217.mlab.com:59217/story-toaster"
    //host: 'ds159217.mlab.com',
    //port: 59217,
    //username: 'stapi',
    //password: '3t0ryt0a1Ajaxu7Q',
    //database: 'story-toaster'

    // user name: stapi
    // password:  3t0ryt0a1Ajaxu7Q

    //mongo ds159217.mlab.com:59217/story-toaster -u <dbuser> -p <dbpassword>
    // mongodb://<dbuser>:<dbpassword>@ds159217.mlab.com:59217/story-toaster
  },

  mongoAltas: {
    adapter: 'sails-mongo',
    url: 'mongodb://stapi:j0P12L6lSOpNuTdi@cluster0-shard-00-00-5ihh8.mongodb.net:27017,cluster0-shard-00-01-5ihh8.mongodb.net:27017,cluster0-shard-00-02-5ihh8.mongodb.net:27017/stapi?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin'
    , ssl: true
  }

}
